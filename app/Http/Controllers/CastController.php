<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        
        return view('casts.all', compact('casts'));
    }

    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('casts')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);
        return redirect ('/cast/create')->with('success', 'Cast has been successfully created!');
    }

    public function show($cast_id)
    {
        $query = DB::table('casts')->where('id', $cast_id)->first();
        return view('casts.show', compact('query'));
    }
    
    public function edit($cast_id)
    {
        $query = DB::table('casts')->where('id', $cast_id)->first();
        return view('casts.edit', compact('query'));
    }

    public function update($cast_id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = DB::table('casts')->where('id', $cast_id)->
                                    update([
                                        'nama' => $request["nama"],
                                        'umur' => $request["umur"],
                                        'bio' => $request["bio"]
                                    ]);
        return redirect ("/cast/$cast_id")->with('success', 'Cast has been successfully edited!');
    }

    public function destroy($cast_id)
    {
        $query = DB::table('casts')->where('id', $cast_id)->delete();
        return redirect ('/cast')->with('success', 'Cast has been successfully deleted!');
    }
}
