@extends('layouts.main')

@section('subjudul')
    {{ $query->nama }}
@endsection

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}    
        </div>
    @endif
    <div class="col-12">
        <div class="d-flex justify-content-end">
            <a href="/cast/{{ $query->id }}/edit"><button class="btn btn-success btn-md">Edit</button></a>
        </div> 
        <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
                <thead>
                <tr>
                    <th>ID</th>
                    <td>{{ $query->id }}</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Nama</th>
                    <td>{{ $query->nama }}</td>
                </tr>
                <tr>
                    <th>Umur</th>
                    <td>{{ $query->umur }}</td>
                </tr>
                <tr>
                    <th>Bio</th>
                    <td>{{ $query->bio }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection