@extends('layouts.main')

@section('subjudul')
    Edit Cast
@endsection

@section('content')
        <form action="/cast/{{ $query->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Name</label>
                    <input type="text" class="form-control" id="nama" value="{{ old('nama', $query->nama)}}" name="nama" placeholder="Enter Name">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Age</label>
                    <input type="text" class="form-control" id="umur" value="{{ old('nama', $query->umur)}}" name="umur" placeholder="Enter Age">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" value="{{ old('nama', $query->bio)}}" name="bio" placeholder="Enter Bio">
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-success" style="margin-right: 1.5rem">Edit</button>
            </div>
        </form>
@endsection