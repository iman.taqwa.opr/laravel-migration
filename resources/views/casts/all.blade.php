@extends('layouts.main')

@section('subjudul')
    All Casts
@endsection

@section('content')
@if (session('success'))
  <div class="alert alert-success">
    {{ session('success') }}    
  </div>
@endif
<div class="col-12">
    <div class="d-flex justify-content-end">
      <a href="/cast/create"><button class="btn btn-success btn-md">Create New Cast</button></a>
    </div>  
      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Age</th>
              <th>Bio</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($casts as $key => $cast)
              <tr>
                <td> {{ $cast->id }} </td>
                <td> {{ $cast->nama }} </td>
                <td> {{ $cast->umur }} </td>
                <td> {{ $cast->bio }} </td>
                <td class="d-flex inline">
                  <a href="/cast/{{ $cast->id }}"><button class="btn btn-primary btn-sm">Show</button></a>
                  <a href="/cast/{{ $cast->id }}/edit"><button class="btn btn-sm">Edit</button></a>
                  <form action="/cast/{{ $cast->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="5" align="center"> No data </td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
@endsection