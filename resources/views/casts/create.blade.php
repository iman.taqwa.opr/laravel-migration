@extends('layouts.main')

@section('subjudul')
    Create New Cast
@endsection

@section('content')
        @if (session('success'))
            <div class="alert alert-success">
            {{ session('success') }}    
            </div>
        @endif
        

        
        <form action="/cast" method="POST">
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Name</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Enter Name">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Age</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', '') }}" placeholder="Enter Age">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Enter Bio">
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-primary" style="margin-right: 1.5rem">Create</button>
            </div>
        </form>
@endsection